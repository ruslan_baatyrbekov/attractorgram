<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhotoController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('user.photo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(PhotoRequest $request)
    {
        $photo = new Photo();
        if($request->hasFile('picture')){
            $photo->picture = $request->file('picture')->store('photos','public');
        }
        $photo->description = $request->input('description');
        $photo->user_id = Auth::user()->id;
        $photo->save();
        return redirect()->route('user.profile')->with('status','Photo succsessfuly added!');
    }
}
