<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function follow(User $user)
    {
        $boolean = false;
        $followings = Auth::user()->seconduser;
        foreach ($followings as $follow){
            if ($follow->id == $user->id) {
                $boolean = true;
            }
        }
        if ($boolean){
            return redirect()->route('user.feed')->with('error', 'you are already subscribed to '.$user->name.'!');
        }else{
            Auth::user()->seconduser()->attach(User::find($user->id));
            return redirect()->route('user.feed')->with('success', 'you have successfully subscribed to '.$user->name.'!');
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unfollow(User $user)
    {
        $boolean = false;
        $followings = Auth::user()->seconduser;
        foreach ($followings as $follow){
            if ($follow->id == $user->id) {
                $boolean = true;
            }
        }
        if (!$boolean){
            return redirect()->route('user.feed')->with('error', 'you are already unsubscribed to '.$user->name.'!');
        }else{
            Auth::user()->seconduser()->detach(User::find($user->id));
            return redirect()->route('user.feed')->with('success', 'you have successfully unsubscribed to '.$user->name.'!');
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUnfollow(User $user)
    {
        $boolean = false;
        $followings = Auth::user()->seconduser;
        foreach ($followings as $follow){
            if ($follow->id == $user->id) {
                $boolean = true;
            }
        }
        if (!$boolean){
            return redirect()->route('user.profile')->with('error', 'you are already unsubscribed to '.$user->name.'!');
        }else{
            Auth::user()->seconduser()->detach(User::find($user->id));
            return redirect()->route('user.profile')->with('success', 'you have successfully unsubscribed to '.$user->name.'!');
        }
    }

    /**
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function like(Photo $photo)
    {
        $likes = 0;
        foreach(Auth::user()->photolikes as $like)
        {
            if($like->id !== $photo->id){
                $likes = 0;
            }else{
                $likes++;
            }
        }
        if($likes > 0){
            Auth::user()->photolikes()->detach($photo->id,['likes' => true]);
            return redirect()->route('user.profile')->with('error', 'dislike');
        }else{
            Auth::user()->photolikes()->attach($photo->id,['likes' => true]);
            return redirect()->route('user.feed')->with('success', 'like');
        }

    }

    /**
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profilelike(Photo $photo)
    {
        $likes = 0;
        foreach(Auth::user()->photolikes as $like)
        {
            if($like->id !== $photo->id){
                $likes = 0;
            }else{
                $likes++;
            }
        }
        if($likes > 0){
            Auth::user()->photolikes()->detach($photo->id,['likes' => true]);
            return redirect()->route('user.profile')->with('error', 'dislike');
        }else{
            Auth::user()->photolikes()->attach($photo->id,['likes' => true]);
            return redirect()->route('user.profile')->with('success', 'like');
        }

    }

}
