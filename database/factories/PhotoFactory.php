<?php

namespace Database\Factories;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Photo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'picture' => $this->getImage(rand(1,17)),
            'description' => $this->faker->text(10),
            'user_id' => rand(1,5)
        ];
    }

    private function getImage($image_number = 1)
    {
        $path = storage_path() . "/seeds/photos/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(400)->encode('jpg');
        Storage::disk('public')->put('photos/'.$image_name, $resize->__toString());
        return 'photos/'.$image_name;
    }
}
