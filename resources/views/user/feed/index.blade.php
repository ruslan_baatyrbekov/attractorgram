@extends('layouts.app')

@section('content')
    @include('notifications.alerts')
    <div class="row-cols-1">
        @for ($i = 0; $i < count($users); $i++)
            @if($users[$i]->id !== \Illuminate\Support\Facades\Auth::user()->id)
            <div class="card mb-3" style="width: 40rem;">
                <div class="card-header">
                    {{$users[$i]->name}}
                    <form action="{{route('user.follow',['user' => $users[$i]])}}" method="post" class="dash-form">
                        @csrf
                        <button class="btn btn-sm btn-info btn-lg dashboard-bt" type="submit">Follow</button>
                    </form>
                    <form action="{{route('user.unfollow',['user' => $users[$i]])}}" method="post" class="dash-form">
                        @csrf
                        <button class="btn btn-sm btn-dark btn-lg dashboard-bt" type="submit">Unfollow</button>
                    </form>
                </div>
                <div id="carouselExampleControls-{{$i}}" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($users[$i]->photo as $photo)
                            <li data-target="#carouselExampleIndicators"  data-slide-to="{{ $loop->index }}" class="{{$loop->first ? 'active' : ''}}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        @foreach($users[$i]->photo as $photo)
                            <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                                <div class="card" style="width: 40rem;">
                                    <img src="{{asset('/storage/' . $photo->picture)}}" class="card-img-top" alt="...">
                                    <div class="card-body row">
                                        <div class="col-2">
                                            <p class="card-text">{{$photo->description}}</p>
                                        </div>
                                        <div class="col-2">
                                            <form action="{{route('user.like',['photo' => $photo])}}" method="post" class="dash-form">
                                                @csrf
                                                <button class="btn btn-sm btn-dark btn-lg dashboard-bt" type="submit"><i class="fas fa-heart"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-footer text-muted row alert-dark">
                                        <p class="m-1">Likes: {{count($photo->userlikes)}}</p>
                                    </div>
                                    <footer class="blockquote-footer">
                                        <p class="ml-5">
                                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample-{{$photo->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                Comments
                                            </a>
                                        </p>
                                        <div class="collapse" id="collapseExample-{{$photo->id}}">
                                            @foreach($photo->comment as $comment)
                                                <div>
                                                    <p class="border-dark"><b>{{$comment->user->name}}:</b> {{$comment->body}}</p>
                                                </div>
                                            @endforeach
                                        </div>
                                    </footer>

                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls-{{$i}}" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls-{{$i}}" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            @endif
        @endfor
    </div>
@endsection
