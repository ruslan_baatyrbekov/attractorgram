@extends('layouts.app')

@section('content')
    <form enctype="multipart/form-data" method="post" action="{{route('user.photo.store')}}">
        @csrf
        <div class="form-group">
            <label for="description" class="mt-1">Description:</label>
            <input type="text" class="form-control" id="description" name="description">
            @error('description')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
            <label for="picture" class="mt-1">Photo:</label>
            <div class="form-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="picture">
                    <label class="custom-file-label" for="customFile">Add Picture</label>
                </div>
            </div>
            @error('picture')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-secondary">Save</button>
    </form>
@endsection



