@extends('layouts.app')

@section('content')
    @include('notifications.alerts')
    <div>
        <h1>My photos</h1>
    </div>
    <a href="{{route('user.photo.create')}}" type="button" class="btn btn-primary mb-3">Add new photo</a>
    <div class="row row-cols-lg-4 row-cols-md-4 row-cols-sm-2">
        @foreach($user->photo as $photo)
        <div class="card m-2" style="width: 20rem;">
            <img src="{{asset('/storage/' . $photo->picture)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{$photo->description}}</h5>
                <p class="m-1">Likes: {{count($photo->userlikes)}}</p>
            </div>
        </div>
        @endforeach
    </div>
    <div>
        <h2>Followings</h2>
        @foreach($followings as $follow)
                <div class="card mb-3" style="width: 40rem;">
                    <div class="card-header">
                        {{$follow->name}}
                        <form action="{{route('user.profileunfollow',['user' => $follow])}}" method="post" class="dash-form">
                            @csrf
                            <button class="btn btn-sm btn-dark btn-lg dashboard-bt" type="submit">Unfollow</button>
                        </form>
                    </div>
                    <div id="carouselExampleControls-{{$follow->id}}" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($follow->photo as $photo)
                                <li data-target="#carouselExampleIndicators"  data-slide-to="{{ $loop->index }}" class="{{$loop->first ? 'active' : ''}}"></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            @foreach($follow->photo as $photo)
                                <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                                    <div class="card" style="width: 40rem;">
                                        <img src="{{asset('/storage/' . $photo->picture)}}" class="card-img-top" alt="...">
                                        <div class="card-body row">
                                            <div class="col-2">
                                                <p class="card-text">{{$photo->description}}</p>
                                            </div>
                                            <div class="col-2">
                                                <form action="{{route('user.profilelike',['photo' => $photo])}}" method="post" class="dash-form">
                                                    @csrf
                                                    <button class="btn btn-sm btn-dark btn-lg dashboard-bt" type="submit"><i class="fas fa-heart"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="card-footer text-muted row alert-dark">
                                            <p class="m-1">Likes: {{count($photo->userlikes)}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls-{{$follow->id}}" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls-{{$follow->id}}" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
        @endforeach
    </div>
@endsection
