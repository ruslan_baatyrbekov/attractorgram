<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\MainController::class, 'index'])->name('user.feed');
Route::get('/profile', [App\Http\Controllers\MainController::class, 'profile'])->name('user.profile');
Route::post('follow/{user}/',[App\Http\Controllers\UsersController::class,'follow'])->name('user.follow');
Route::post('unfollow/{user}/',[App\Http\Controllers\UsersController::class,'unfollow'])->name('user.unfollow');
Route::post('profileunfollow/{user}/',[App\Http\Controllers\UsersController::class,'profileUnfollow'])->name('user.profileunfollow');
Route::post('like/{photo}/',[App\Http\Controllers\UsersController::class,'like'])->name('user.like');
Route::post('profilelike/{photo}/',[App\Http\Controllers\UsersController::class,'profilelike'])->name('user.profilelike');
Route::get('photo/create',[\App\Http\Controllers\PhotoController::class,'create'])->name('user.photo.create');
Route::post('photo/store',[\App\Http\Controllers\PhotoController::class,'store'])->name('user.photo.store');
